<?php
/**
 * Created by PhpStorm.
 * User: deepak
 * Date: 18/8/18
 * Time: 2:28 AM
 */

class Constants
{


    /**
     * Constants constructor.
     */
    private function __construct()
    {
    }

    public static $SPACE = " ";
    public static $BLANK = "";
    public static $SIDE_BAR_MENU_CLASS_ACTIVE = "active";
    public static $SIDE_BAR_MENU_COLLAPSE_SHOW = "show";


}