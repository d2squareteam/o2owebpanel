<?php include_once ("../Utils/Utils.php");?>
<!DOCTYPE html>
<html lang="en">

<head>

    <title>Manage Modules</title>
    <?php include_once("../header-library.php"); ?>
</head>

<body class="">


<div class="wrapper ">
    <?php include_once("../sidebar.php"); ?>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                            <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <a class="navbar-brand" href="#pablo">Manage Modules</a>
                </div>
                <?php include_once("../search-bar.php"); ?>
            </div>
        </nav>
        <!-- End Navbar -->


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">assignment</i>
                                </div>
                                <h4 class="card-title">All Modules</h4>
                            </div>
                            <div class="card-body">
                                <div class="toolbar">
                                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                                </div>
                                <div class="material-datatables">
                                    <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                           cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>ID#</th>
                                            <th>Group</th>
                                            <th>Bliz ID</th>
                                            <th>Host ID</th>
                                            <th>Status</th>
                                            <th class="disabled-sorting text-center">Actions</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID#</th>
                                            <th>Group</th>
                                            <th>Bliz ID</th>
                                            <th>Host ID</th>
                                            <th>Status</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        <tr>
                                            <td>**********</td>
                                            <td>ABCD</td>
                                            <td>**********</td>
                                            <td>**********</td>
                                            <td>Reset</td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-link btn-danger btn-just-icon remove" title="Delete" onclick=demo.showSwal('warning-message-and-cancel')><i
                                                            class="material-icons">close</i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>**********</td>
                                            <td>ABCD</td>
                                            <td>**********</td>
                                            <td>**********</td>
                                            <td>Reset</td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-link btn-danger btn-just-icon remove" title="Delete" onclick=demo.showSwal('warning-message-and-cancel')><i
                                                            class="material-icons" >close</i></a>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end content-->
                        </div>
                        <!--  end card  -->
                    </div>
                    <!-- end col-md-12 -->
                </div>
                <!-- end row -->
            </div>

        </div>

        <footer class="footer">
            <?php include_once("../footer.php"); ?>
        </footer>


    </div>

</div>
<script src="../../assets/js/datatable/datatable.js"></script>
</body>
</html>
