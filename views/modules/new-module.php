<?php include_once ("../Utils/Utils.php");?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <?php include_once("../header-library.php"); ?>
    <title>New Module</title>
</head>
<body class="">

<div class="wrapper ">
    <?php include_once("../sidebar.php"); ?>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                            <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <a class="navbar-brand" href="#pablo">New Module</a>
                </div>
                <?php include_once("../search-bar.php"); ?>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-12 mr-auto ml-auto">
                        <div class="card-shadow">
                            <form id="RegisterValidation" action="#" method="">
                                <div class="card">
                                    <div class="card-header card-header-rose card-header-icon">
                                        <div class="card-icon">
                                            <i class="material-icons">contacts</i>
                                        </div>
                                        <h4 class="card-title">Create new module</h4>
                                    </div>
                                    <div class="card-body ">
                                        <div class="form-group">
                                            <label for="moduleId" class="bmd-label-floating">Module ID</label>
                                            <input type="text" class="form-control" id="moduleId" required="true">
                                        </div>
                                        <div class="form-group">
                                            <label for="blizzId" class="bmd-label-floating">Blizz ID</label>
                                            <input type="text" class="form-control" id="blizzId" required="true">
                                        </div>
                                        <div class="form-group">
                                            <label for="hostId" class="bmd-label-floating">Host ID</label>
                                            <input type="text" class="form-control" id="hostId" required="true">
                                        </div>
                                        <div class="form-group">
                                            <select class="selectpicker" data-size="7"
                                                    data-style="btn btn-primary btn-round" title="Single Select">
                                                <option disabled selected>Select Group</option>
                                                <option value="2">Admin</option>
                                                <option value="3">Manager</option>
                                                <option value="4">Supervisor</option>
                                            </select>
                                        </div>
                                        <div class="category form-category">* Required fields</div>
                                    </div>
                                    <div class="card-footer ml-auto mr-auto">
                                        <button type="submit" class="btn btn-rose">Add Module</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <?php include_once("../footer.php"); ?>
    </footer>
</div>

</body>
</html>
