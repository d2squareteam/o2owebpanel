<?php
/**
 * Created by PhpStorm.
 * User: deepak
 * Date: 18/8/18
 * Time: 2:03 AM
 */

include_once("../enums/UrlSegment.php");
include_once("../common/Constants.php");

class Utils
{


    public static function getSegmentFromUrl()
    {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        return $uri_segments[3];
    }

    public static function sideBarGroupMenuClassActive()
    {
        switch (self::getSegmentFromUrl()) {
            case UrlSegment::Users:
            case UrlSegment::Modules:
                return Constants::$SPACE . Constants::$SIDE_BAR_MENU_CLASS_ACTIVE;
            default:
                return Constants::$BLANK;
        }
    }

    public static function getServerScriptName()
    {
        return basename($_SERVER['SCRIPT_NAME']);
    }

    public static function sideBarChildMenuClassActive()
    {
        /*if (!strcmp(self::getServerScriptName(), UrlSegment::NewUser)) {
            return Constants::$SPACE . Constants::$SIDE_BAR_MENU_CLASS_ACTIVE;
        } else {
            if (!strcmp(self::getServerScriptName(), UrlSegment::NewModule)) {
                return Constants::$SPACE . Constants::$SIDE_BAR_MENU_CLASS_ACTIVE;
            } else {
                if (!strcmp(self::getServerScriptName(), UrlSegment::ManageUser)) {
                    return Constants::$SPACE . Constants::$SIDE_BAR_MENU_CLASS_ACTIVE;
                } else {
                    return Constants::$BLANK;
                }

            }

        }*/
    }

    public static function isMenuCollapse()
    {
        if (!strcmp(trim(self::sideBarGroupMenuClassActive()), Constants::$SIDE_BAR_MENU_CLASS_ACTIVE))
            return true;
        else
            return false;
    }

    public static function showCollapseMenu()
    {
        if (!strcmp(trim(self::sideBarGroupMenuClassActive()), Constants::$SIDE_BAR_MENU_CLASS_ACTIVE))
            return Constants::$SPACE . Constants::$SIDE_BAR_MENU_COLLAPSE_SHOW;
        else
            return Constants::$BLANK;
    }


}