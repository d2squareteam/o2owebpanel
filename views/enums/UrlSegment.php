<?php
/**
 * Created by PhpStorm.
 * User: deepak
 * Date: 18/8/18
 * Time: 2:30 AM
 */

abstract class UrlSegment
{

    const Users = "users";
    const Modules = "modules";

    const NewUser="new-user.php";
    const NewModule="new-module.php";
    const ManageUser="manage-user.php";
    const ManageModule="manage-module.php";
}